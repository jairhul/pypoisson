import matplotlib.pyplot as _plt
import numpy as _np
import Poisson as _Poisson

def Plot(arg, title='', colorbar=True):
    """
    Plot an SF7 instance or file. If the argument is a string,
    it will be loaded using the SF7 class and then plotted.
    """
    a = _Poisson._EnsureItsSF7(arg)

    # make a quiver plot
    _plt.quiver(a['X'],a['Y'],a['Bx'],a['By'],a['|B|'],cmap=_plt.cm.magma)
    _plt.xlabel('X ('+a.unitsdict['X']+')')
    _plt.ylabel('Y ('+a.unitsdict['Y']+')')
    if colorbar:
        _plt.colorbar()
    if title != '':
        _plt.title(title)
    else:
        _plt.title(a.filename)
    _plt.tight_layout()
    _plt.axes().set_aspect('equal', 'datalim')

def PlotQuiver(arg,title=''):
    """
    Plot an SF7 instance or file. If the argument is a string,
    it will be loaded using the SF7 class and then plotted.
    """
    Plot(arg,title)

def PlotStream(arg, title='', colorbar=True):
    """
    Plot an SF7 instance or file. If the argument is a string,
    it will be loaded using the SF7 class and then plotted.
    """
    a = _Poisson._EnsureItsSF7(arg)

    xa = _np.sort(_np.array(list(set(a['X']))))
    ya = _np.sort(_np.array(list(set(a['Y']))))
    bx = a['Bx'].reshape(a.nx,a.ny)
    by = a['By'].reshape(a.nx,a.ny)
    absb = a['|B|'].reshape(a.nx,a.ny)
    _plt.streamplot(xa,ya,bx,by,color=absb,cmap=_plt.cm.magma)
    _plt.xlabel('X ('+a.unitsdict['X']+')')
    _plt.ylabel('Y ('+a.unitsdict['Y']+')')
    if colorbar:
        _plt.colorbar()
    if title != '':
        _plt.title(title)
    else:
        _plt.title(a.filename)
    _plt.tight_layout()
    _plt.axes().set_aspect('equal', 'datalim')

def PlotFull(arg,title=''):
    """
    Plot both a quiver and stream plot on top of each other.
    """
    Plot(arg,title)
    PlotStream(arg,title,False)
