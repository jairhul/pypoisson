from setuptools import setup

setup(
   name='pypoisson',
   version='1.0',
   description='BDSIM Poisson python helper package',
   author='JAI@RHUL',
   author_email='stewart.boogert@rhul.ac.uk',
   package_dir = {'pypoisson': './'},
   packages=['pypoisson'],
)
