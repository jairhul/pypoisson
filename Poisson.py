import numpy as _np
import tarfile as _tarfile

import Plot as _Plot

class Poisson :
    def __init__(self, filename) :
        pass

    def draw(self) :
        pass

class SF7(object):
    """
    Poisson SuperFish 7 SF7 format loader.

    a = SF7("filename.txt")
    or
    a = SF7()
    a.Load("filename.txt")

    Have a look at:
    a.columns
    a.unitsdict
    a.Plot()

    Access data:
    a[0]   # get 0th row or data
    a['X'] # get column with label 'X'
    a.GetColum('X')
    a.GetRow(12)

    """
    def __init__(self, filename=None):
        object.__init__(self)
        self.filename = filename
        self.xMin      = 0
        self.yMin      = 0
        self.xMax      = 0
        self.yMax      = 0
        self.nx        = 0
        self.ny        = 0
        self.columns   = []
        self.units     = []
        self.unitsdict = {}
        self.data      = _np.array([])
        self.xStep     = 0 # calcualted from xmin,xmax,nx
        self.yStep     = 0

        if type(filename) == str:
            self.Load(filename)

    def Load(self, filename):
        if ('tar' in filename) or ('gz' in filename):
            print 'pypoisson.SF7.Load> zipped file'
            tar = _tarfile.open(filename,'r')
            f = tar.extractfile(tar.firstmember)
        else:
            print 'pypoisson.SF7.Load> normal file'
            f = open(filename)

        intoData      = False
        passedColumns = False
        dataraw       = []
        for line in f:
            splitline = line.strip().split()
            sl        = splitline # shortcut

            # skip empty lines
            if len(sl) == 0:
                continue
            
            # detect header info
            if not intoData:
                if sl[0].startswith('(Xmin'):
                    self.xMin, self.yMin = map(float, sl[-1].strip('(').strip(')').split(','))
                elif sl[0].startswith('(Xmax'):
                    self.xMax, self.yMax = map(float, sl[-1].strip('(').strip(')').split(','))
                elif 'X and Y' in line:
                    self.nx = int(sl[-2]) + 1
                    self.ny = int(sl[-1]) + 1
                elif sl[0] == 'X': # column row
                    self.columns = sl
                    passedColumns = True
                if passedColumns and sl[0].startswith('('): # units row
                    units = [u.strip('(').strip(')') for u in sl]
                    self.units = units
                    self.unitsdict = dict(zip(self.columns,self.units))
                    intoData = True
            else:
                # main data
                dataraw.append(sl)

        f.close()
        self.data = _np.array(dataraw,dtype=float)
        del dataraw #explictly delete

        self.xStep = (self.xMax - self.xMin) / self.nx
        self.yStep = (self.yMax - self.yMin) / self.ny

    def __repr__(self):
        s = ''
        s += 'pypoisson.SF7 instance\n'
        s += str(self.nx) + ' x ' + str(self.ny) + ' entries\n'
        return s

    def __len__(self):
        return _np.shape(self.data)[0]

    def GetColumn(self, columnName):
        if columnName not in self.columns:
            raise ValueError('Invalid column name '+columnName)
        ind = self.columns.index(columnName)
        return self.data[:,ind]

    def GetRow(self, index):
        if index > _np.shape(self.data)[1]:
            raise ValueError('Invalid index '+str(index))
        return self.data[index,:]

    def __getitem__(self, arg):
        t = type(arg)
        if (t == str):
            return self.GetColumn(arg)
        elif (t == int):
            return self.GetRow(arg)

    def Plot(self):
        """ Plot a quiver plot showing the fields"""
        _Plot.Plot(self)

    def PlotStream(self):
        """ Plot a stream plot showing the fields"""
        _Plot.PlotStream(self)

    def PlotFull(self):
        """Plot both a stream and quiver plot showing the fields"""
        _Plot.PlotFull(self)


def _EnsureItsSF7(arg):
    if (type(arg) == str):
        a = SF7(arg)
    elif (type(arg) == SF7):
        a = arg
    else:
        raise ValueError('Not an SF7 instance')
    return a
