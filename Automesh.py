import matplotlib.pyplot as _plt
import re as _re
import numpy as _np
import pygeometry.geant4 as _g4
import pygeometry.vtk as _vtk
import pygeometry.gdml as _gdml
import numpy as _np
import cProfile as _cp
import gc as _gc
import heapq as _heapq
from pygeometry.geant4.solid.TwoVector import TwoVector as _TwoVector
from collections import OrderedDict as _OrderedDict

def splitPair(keyEqualVal) :
    s = keyEqualVal.split("=")
    return [s[0],float(s[1])]

def hyperbola(r, x) :
    '''
       :param r poisson like radius for hyperbola :
       :param x position to evaluate hyperbola :
    '''
    return r**2/2/x

def axisDistanceOrdered(axisAngle, points) :
    '''
        :param axisAngle : angle of straight line to check distance:
        :param points : array of 2D points :
    '''


class Automesh :
    ''' Class to perform operations on automesh files'''

    def __init__(self, filename) :
        '''
        Contructor for Automesh object :filename: automesh *.am file
        '''

        self.filename = filename
        self.regList         = [] # list of regions and parameters
        self.matList         = [] # list of materials for each region
        self.regPointList    = [] # list of regions, list of points
        self.regDict = _OrderedDict()
        self.hulDict = _OrderedDict()

        self.readAndParse(self.filename)

        # Number of points to evaluate hyperbola
        self.nHyperbolaPoint = 6
        self.nCirclePoint    = 6

        self.constructHulls()
        self.hulReflect(self.regDict['reg0']['reg']['angle']/180.0*_np.pi)
        self.extent()

    def readAndParse(self,filename) :
        ''' Parse automesh file'''

        f = open(filename)

        # read entire file
        readFile = f.read()

        # remove comments (from ; to \n)
        readFile = _re.sub(';.*?\n','',readFile)

        # remove new lines and carrage returns
        readFile = readFile.replace('\n',' ').replace('\r',' ')

        # split read file
        splitFile = readFile.split("&")

        # remove empty lines
        splitFile = filter(lambda s : len(s.strip()) != 0, splitFile)

        # remove white space
        splitFile = [s.strip() for s in splitFile]

        # remove commas
        splitFile = [s.replace(',',' ') for s in splitFile]

        # make all lower case
        splitFile = [s.lower() for s in splitFile]

        # break down each command
        splitFile = [s.split() for s in splitFile]

        # problem name
        self.name = splitFile[0]

        # scan for regions extract parameters and points for region
        regPointList = []
        first     = True
        for c in splitFile[1:] : # first line is problem name
            if c[0] == 'reg' :
                self.regList.append(c)
                a,b=c[1].split("=")
                self.matList.append([a,b])
                if not first :
                    self.regPointList.append(regPointList)
                regPointList = []
                first     = False
            else :
                regPointList.append(c)

        self.regPointList.append(regPointList)

        # build dictionary of regions
        for i in range(len(self.regList)) :
            d = {}
            d['reg'] = {}
            for rl in self.regList[i][1:] :
                k,v = splitPair(rl)
                d['reg'][k] = v

            d['points'] = []
            for pl in self.regPointList[i] :
                pl_temp = {}

                for ppl in pl[1:] :
                    k,v = splitPair(ppl)
                    pl_temp[k] = v

                d['points'].append(pl_temp)
            self.regDict['reg'+str(i)] = d


    def draw(self, hullKey=None):

        if hullKey == None:
            hullKey = self.hulDict.keys()
        elif type(hullKey == str):
            hullKey = [hullKey]

        _plt.figure()
        for k in hullKey:
            v = self.hulDict[k]
            _plt.plot(v[0], v[1], "+-", label=k)
            _plt.gca().fill(v[0],v[1],fill='\\',alpha=0.5)

        _plt.legend()
        
    def constructHulls(self) :
        ''' Matplotlib 2D section of automesh input file'''
        # loop over regions and draw
        for k in self.regDict.keys() :
            x = []
            y = []
            # loop over region points
            for p in self.regDict[k]['points'] :
                nt = 0
                try :
                    nt = p['nt']
                except KeyError:
                    nt = 1
                if nt == 1 :
                    x.append(p['x'])
                    y.append(p['y'])
                    
                elif nt == 2 :
                    x0 = p['x0']
                    y0 = p['y0']
                    try :
                        x1 = p['x']
                        y1 = p['y']
                    except KeyError:
                        x1 = 0
                        y1 = 0
                    try :
                        A=p['a']
                    except KeyError:
                        A=0
                    try :
                        B=p['b']
                    except KeyError:
                        B=0
                    try :
                        AOVRB=p['aovrb']
                    except KeyError :
                        AOVRB=0
                    try :
                        R=p['r']
                    except KeyError :
                        try :
                            R=p['radius']
                        except KeyError :
                            R=0
                    try :
                        THETA=p['theta']
                    except KeyError :
                        THETA=0
                        
                    xz = x[-1]
                    yz = y[-1]
                    phimin = _np.arctan((yz-y0)/(xz-x0)) + _np.pi 
                    phimax = _np.arctan((y1-y0)/(x1-x0)) + _np.pi 
                    p = _np.linspace(phimin,phimax,self.nCirclePoint)
                    xp = R*_np.cos(p) + x0
                    yp = R*_np.sin(p) + y0
                    x.extend(xp[1:])
                    y.extend(yp[1:])

					
                elif nt == 3:
                    # TODO hyperbola points cannot actually be start and end as give degenerate meshing for G4
                    x0   = x[-1]
                    x1   = p['x']
                    hx   = _np.linspace(x0,x1,self.nHyperbolaPoint)
                    hy   = hyperbola(p['r'],_np.linspace(x0,x1,self.nHyperbolaPoint))
                    x.extend(hx[1:-1])
                    y.extend(hy[1:-1])
                elif nt == 4 or nt == 5:
                    x0   = x[-1]
                    y0   = y[-1]
                    x1   = p['x']
                    y1   = p['y']
                    phi0 = _np.arctan2(y0,x0)
                    phi1 = _np.arctan2(y1,x1)
                    r0   = _np.sqrt(x0**2+y0**2)
                    r1   = _np.sqrt(x1**2+y1**2)

                    phiMin = min(phi0,phi1)
                    phiMax = max(phi0,phi1)

                    if nt == 4 :
                        p = _np.linspace(phiMin,phiMax,self.nCirclePoint)
                    elif nt == 5 :
                        p = _np.linspace(phiMax,phiMin,self.nCirclePoint)

                    cx = r0*_np.cos(p)
                    cy = r0*_np.sin(p)

                    x.extend(cx[1:])
                    y.extend(cy[1:])

            x = [10*xi for xi in x]
            y = [10*yi for yi in y]
            
            self.hulDict[k] = [x,y]
            self.shrinkFromRefelctionAxis(k, self.regDict['reg0']['reg']['angle'])
            self.shrinkFromxAxis(k)

    def shrinkFromRefelctionAxis(self, hullName, angle, tolerance=1e-6):
        """
        Shrinks points from axis of rotation by 1 nm.
        This ensures no overlaps.
        """
        d = self.hulDict[hullName]
        L = _TwoVector(40,0)
        L = L.Rotated(angle)
        dx = []
        dy = []
        
        for x,y in zip(d[0], d[1]):
            if x == 0 and y == 0:
                dx.append(tolerance)
                dy.append(tolerance)
                continue

            p = _TwoVector(x, y)
            
            dist = p - ((p.dot(L))/((abs(p))**2)) * p
        
            if dist <= tolerance:
                p = p + tolerance
                dx.append(p.x)
                dy.append(p.y)
            else:
                dx.append(p.x)
                dy.append(p.y)
                
        dout = [dx,dy]
        self.hulDict[hullName] = dout #replace

    def shrinkFromxAxis(self, hullName, tolerance=1e-6):
        """
        Strinks points from x axis by 1 nm.
        This ensures no overlaps.
        """
        d = self.hulDict[hullName]
        L = _TwoVector(40,0)
        dx = []
        dy = []
        
        for x,y in zip(d[0], d[1]):

            if y == 0:
                dx.append(x)
                dy.append(tolerance)
                continue

            p = _TwoVector(x, y)
            
            dist = p - ((p.dot(L))/((abs(p))**2)) * p
            
            if dist <= tolerance:
                p = p + tolerance
                dx.append(p.x)
                dy.append(p.y)
            else:
                dx.append(p.x)
                dy.append(p.y)
                
        dout = [dx,dy]
        self.hulDict[hullName] = dout #replace
            
    def hulReflect(self, angle = _np.pi/2):
        """
        Make all the reflected hulls for the symmetry for the current file.
        Needed as reflections around an arbitary axis cannot be performed 
        in gdml.
        
        c(2t),  s(2t)
        s(2t), -c(2t)
        """

        self.hulReflectDict = {}

        for k in self.hulDict.keys() :
            try:
                angz = 2*(self.regDict['reg0']['reg']['anglez']/180)*_np.pi
            except KeyError:
                angz=0
            xpl = []
            ypl = []
            for i in range(0,len(self.hulDict[k][0])) :
                x = self.hulDict[k][0][i]
                y = self.hulDict[k][1][i]

                xp = _np.cos(2*angle-angz)*x + _np.sin(2*angle-angz)*y
                yp = _np.sin(2*angle-angz)*x - _np.cos(2*angle-angz)*y

                xpl.append(xp)
                ypl.append(yp)


            self.hulReflectDict[k] = [xpl,ypl]


    def extent(self) :
        """
        Find 2D extent of mesh file.
        """

        minx =  1e50
        maxx = -1e50
        miny = 1e50
        maxy = -1e50

        for k in self.hulDict :
            p = self.hulDict[k]
            x = p[0]
            y = p[1]
            rminx = min(x)
            rmaxx = max(x)
            rminy = min(y)
            rmaxy = max(y)

            if rminx < minx :
                minx = rminx
            if rmaxx > maxx :
                maxx = rmaxx
            if rminy < miny :
                miny = rminy
            if rmaxy > maxy :
                maxy = rmaxy


        self.vmin = [minx,miny]
        self.vmax = [maxx,maxy]

        return [self.vmin,self.vmax]


    def writeGdml(self, l=1000) :
        """
        Write GDML file for input into Geant4/BDSIM
        """

        # TODO Very specific for dipoles, need to fix with the correct symetry
        dmax = max(self.vmax[0],self.vmax[1])

        # Geometry tolerance margin - for now setClip below adjusts the size
        # of this, but left in, in case setClip is removed
        dmax += 1e-6
        halfLength = l*0.5 + 1e-6
        
        world_solid  = _g4.solid.Box("world_solid",dmax,dmax,halfLength)
        bp_solid     = _g4.solid.Tubs("bp_solid",0,15,l,0,2*_np.pi)
        world_sub    = _g4.solid.Subtraction("world_sub",world_solid,bp_solid,[[0,0,0],[0,0,0]])

        worldLogical = _g4.LogicalVolume(world_sub,"G4_Galactic","worldLogical")

        # Moved angle calls to before defining regions in order to do generation & placement in one loop
        anglerad   = self.regDict['reg0']['reg']['angle']/180.*_np.pi*2
        nrot       = int(360./self.regDict['reg0']['reg']['angle']/2)

        #new genwration code, loops through regions and meshes each in turn (material regions only). Supports any number of iron & copper regions.
        for i in range(len(self.matList)) :

            if self.matList[i][0] == "mat":
                regnz = [[-l / 2, [0, 0], 1.0], [l / 2, [0, 0], 1.0]]

                regnx = self.hulDict['reg' + str(i)][0]
                regny = self.hulDict['reg' + str(i)][1]

                regnrefx = self.hulReflectDict['reg' + str(i)][0]
                regnrefy = self.hulReflectDict['reg' + str(i)][1]

                regnv = [[regnx[j], regny[j]] for j in range(len(regnx) - 1)]
                regnrefv = [[regnrefx[j], regnrefy[j]] for j in range(len(regnx) - 1)]

                DefinedSolid = _g4.solid.ExtrudedSolid("DefinedSolid"+str(), regnv, regnz)
                DefinedSolidRef = _g4.solid.ExtrudedSolid("DefinedSolid"+str(i)+"Ref", regnrefv, regnz)

                if self.matList[i][1] == "1": #copper

                    VolumeLogical = _g4.LogicalVolume(DefinedSolid, "G4_Cu", "coilLogical"+str(i))
                    VolumeLogicalRef = _g4.LogicalVolume(DefinedSolidRef, "G4_Cu", "coilLogical"+str(i))

                elif self.matList[i][1] =="2": #iron

                    VolumeLogical = _g4.LogicalVolume(DefinedSolid, "G4_Fe", "yokeLogical"+str(i))
                    VolumeLogicalRef = _g4.LogicalVolume(DefinedSolidRef, "G4_Fe", "YokeLogical" + str(i))

                ang = 0
                for i in range(nrot):
                    suf = str(i)
                    VolumePhysical = _g4.PhysicalVolume([0, 0, ang], [0, 0, 0],
                                                       VolumeLogical,
                                                       "VolumePhysical" + suf,
                                                       worldLogical)
                    VolumePhysicalRef = _g4.PhysicalVolume([0, 0, ang], [0, 0, 0],
                                                       VolumeLogicalRef,
                                                       "VolumePhysicalRef" + suf,
                                                       worldLogical)

                    ang += anglerad

        self.writeGDML(worldLogical, "mag")
        return worldLogical
        

    def writeFluka(self, l=0.1) :
        """
        Write Fluka input file for input in Fluka.
        """
        pass

    def __repr__(self):
        return self.regDict.__repr__()    
    
    def writeEndPieces(self) :
        
        angledeg      = self.regDict['reg0']['reg']['angle']
        angle         = (angledeg)*( _np.pi/180)
        pcoords       = self.hulDict['reg2']
        corecoords    = self.hulDict['reg1']
        extents       = self.extent()
        
        lengthsafety  = 10**(-9)
        pcoords[0].pop()
        pcoords[1].pop()
        npoints       = len(pcoords[0])

    #find closeness of each point to axis, return two closest
        
        closeness = _np.zeros(npoints)
        
        for i in range(npoints) :
            closeness[i] = abs(pcoords[1][i]-pcoords[0][i]* _np.tan(angle))/((1+ _np.sin(angle)**2)**0.5)
                
        points         = _heapq.nsmallest(2,closeness)
        axisoffsetdist = points[0]
        pointa         = closeness.tolist().index(points[0])
        pointb         = closeness.tolist().index(points[1])
        axispoint      = [pcoords[0][pointa],pcoords[1][pointa]]
        axisc          = axispoint[1]-axispoint[0]*axisgradient
        axisangle      = _np.arctan(axisgradient)
        npoles         = int(180/angledeg)
        angletweak     = angle-axisangle
        
        outerpointy    =_heapq.nlargest(1,pcoords[1])
        outerpointx    =_heapq.nlargest(1,pcoords[0])
        outerradius    = (outerpointy[0]**2 + outerpointx[0]**2)**0.5       
        
        airgap         = 2.5 #start finding me properly from the core data
        corepoints     = len(corecoords[0])
        coreoffsetxy   = _np.zeros((2,corepoints))
        coreoffset     = _np.zeros(corepoints)
        
        
        for i in range(corepoints) :
            coreoffsetxy[0][i] = corecoords[0][i]-pcoords[0][0]
            coreoffsetxy[1][i] = corecoords[1][i]-pcoords[0][0]
            coreoffset[i]      = abs((coreoffsetxy[0][i]**2+coreoffsetxy[1][i]**2)**0.5)
                
        coreclosepoints = _heapq.nsmallest(3,coreoffset)
        trncoords       = _np.zeros((2,npoints))
        alpha           = _np.pi/2-axisangle
        
        for i in range(npoints) :
            trncoords[0][i] = (pcoords[0][i])* _np.cos(alpha)-(pcoords[1][i])* _np.sin(alpha)
            trncoords[1][i] = (pcoords[0][i])* _np.sin(alpha)+(pcoords[1][i])* _np.cos(alpha)

        shiftcoords     = _np.zeros((2,npoints))
        mingapx         = _heapq.nsmallest(1,trncoords[0])[0]
        mingapy         = _heapq.nsmallest(1,trncoords[1])[0]
        
        for i in range(npoints) : 
                shiftcoords[0][i] = trncoords[0][i] - mingapx + airgap
                shiftcoords[1][i] = trncoords[1][i] - mingapy
        
        wedgecoords     = _np.zeros((2,npoints))
        
        for i in range(npoints) : 
                wedgecoords[0][i] = (shiftcoords[1][i])
                wedgecoords[1][i] = (shiftcoords[0][i])

        corner_solid            = _g4.solid.GenericPolycone("corner_solid",0,_np.pi/2,npoints,npoints,shiftcoords[0],shiftcoords[1])

        corner_wedge_solid      = _g4.solid.GenericPolycone("corner_wedge_solid",0,angletweak,npoints,npoints,wedgecoords[0],wedgecoords[1])

        corner_solid_ref        = _g4.solid.GenericPolycone("corner_solid",0, -_np.pi/2,npoints,npoints,shiftcoords[0],shiftcoords[1])    

        corner_wedge_solid_ref  = _g4.solid.GenericPolycone("corner_wedge_solid_ref",0,-angletweak,npoints,npoints,wedgecoords[0],wedgecoords[1])




        centrelength     = points[0]-lengthsafety
        centrez          = [[-centrelength,[0,0],1.0],[centrelength,[0,0],1.0]]
        centrexy         = [[shiftcoords[0][i],shiftcoords[1][i]] for i in range(npoints)]

        centre_solid     = _g4.solid.ExtrudedSolid("centre_solid", centrexy, centrez)
        
        world_solid  = _g4.solid.Box("world_solid", 1000,1000,1000)
        bp_solid     = _g4.solid.Tubs("bp_solid",0,mingapy-lengthsafety,1001,0,2*_np.pi)
        world_sub    = _g4.solid.Subtraction("world_sub",world_solid,bp_solid,[[0,0,0],[0,0,0]])
        world_Logical    = _g4.LogicalVolume(world_sub, 'G4_Galactic', 'worldLogical')   
        
        
        
        for i in range (npoles) :

            placeangle  = (2*i+1)*angle + _np.pi/2
                
            offsetangle = placeangle+angle
            placement1  = axisoffsetdist * _np.cos(placeangle) - mingapy * _np.sin(placeangle)
            placement2  = axisoffsetdist * _np.sin(placeangle) + mingapy * _np.cos(placeangle)
            placement3  = -axisoffsetdist * _np.cos(placeangle) - mingapy * _np.sin(placeangle)
            placement4  = -axisoffsetdist * _np.sin(placeangle) + mingapy * _np.cos(placeangle)
            
            cornerLogical      = _g4.LogicalVolume(corner_solid, "G4_Cu", "cornerLogical"+str(i))
            cornerPhysical     = _g4.PhysicalVolume([_np.pi/2,placeangle-angletweak,_np.pi/2],
                                                       [placement1,placement2,0],cornerLogical,
                                                       "cornerPhysical"+str(i),world_Logical)

            cornerLogical_ref  = _g4.LogicalVolume(corner_solid_ref, "G4_Cu", "cornerLogical_ref"+str(i))
            cornerPhysical_ref = _g4.PhysicalVolume([_np.pi/2,placeangle+angletweak,_np.pi/2],
                                                       [placement3,placement4,0],cornerLogical_ref,
                                                       "cornerPhysical_ref"+str(i),world_Logical)
            corner_wedgeLogical      = _g4.LogicalVolume(corner_wedge_solid, "G4_Cu", "corner_wedgeLogical"+str(i))
            corner_wedgePhysical     = _g4.PhysicalVolume([0,0,-(placeangle-angletweak)-_np.pi/2],
                                                        [placement1,placement2,0],corner_wedgeLogical,
                                                        "corner_wedgePhysical"+str(i),world_Logical)
                
            corner_wedgeLogical_ref  = _g4.LogicalVolume(corner_wedge_solid_ref, "G4_Cu", "corner_wedgeLogical_ref"+str(i))
            corner_wedgePhysical_ref = _g4.PhysicalVolume([0,0,-(placeangle+angletweak)-_np.pi/2],
                                                        [placement3,placement4,0],corner_wedgeLogical_ref,
                                                        "corner_wedgePhysical_ref"+str(i),world_Logical)
            
            
            if npoles==6 :
                centreLogical            = _g4.LogicalVolume(centre_solid, "G4_Cu", "centreLogical"+str(i))
                centrePhysical           = _g4.PhysicalVolume([_np.pi/2,placeangle+angle,_np.pi/2],
                                                              [mingapy * _np.cos(offsetangle), mingapy * _np.sin(offsetangle), 0],
                                                              centreLogical,"centrePhysical"+str(i),world_Logical)

            else :
                centreLogical            = _g4.LogicalVolume(centre_solid, "G4_Cu", "centreLogical"+str(i))
                centrePhysical           = _g4.PhysicalVolume([_np.pi/2,placeangle,_np.pi/2],
                                                              [mingapy * _np.cos(placeangle), mingapy * _np.sin(placeangle),0],
                                                              centreLogical,"centrePhysical"+str(i),world_Logical)
                
                
                
        self.writeGDML(world_Logical, "endpieces")
        return world_Logical
            
    def writeGDML(self, worldLogical, outputFileName):
        worldLogical.setClip(1e-6)
      
        _g4.registry.setWorld('worldLogical')

        w = _gdml.Writer()
        w.addDetector(_g4.registry)
        w.write(outputFileName + '.gdml')
        
        return worldLogical


    def Viewer(self, worldLogical):
        worldLogical.setClip(1e-6)
      
        _g4.registry.setWorld('worldLogical')
        
        m = worldLogical.pycsgmesh()

        v = _vtk.Viewer()
        v.addPycsgMeshList(m)
        v.view()
